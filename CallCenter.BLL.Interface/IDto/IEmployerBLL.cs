﻿using CallCenter.SharedEnum;

namespace CallCenter.BLL.Interface.IDto
{
    public interface IEmployerBLL
    {
        int Id { get; }

        EmpPossition IdPossition { get; }

        bool IsFree { get; }

    }
}
