﻿using CallCenter.SharedEnum;

namespace CallCenter.BLL.Interface.IDto
{
    public interface IPossitionBLL
    {
        EmpPossition Title { get; }
    }
}
