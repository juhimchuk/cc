﻿namespace CallCenter.BLL.Interface.IDto
{
    public interface IEmployerInitBLL
    {
        int CountOperator { get; }

        int CountManager { get; }

        int CountSeniorManager { get; }
    }
}
