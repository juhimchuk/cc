﻿using CallCenter.BLL.Interface.IDto;

namespace CallCenter.BLL.Interface.IService
{
    public interface ISeparateService
    {
        IEmployerBLL SeperateCalls();

         void EndCall(IEmployerBLL emp);
    }
}
