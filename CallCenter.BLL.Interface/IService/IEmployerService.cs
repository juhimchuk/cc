﻿using CallCenter.BLL.Interface.IDto;
using CallCenter.SharedEnum;
using System.Collections.Generic;

namespace CallCenter.BLL.Interface.IService
{
    public interface IEmployerService
    {
        void AddRange(IEmployerInitBLL dto);

        void Add(IEmployerBLL dto);

        void Update(IEmployerBLL dto);

        void DeleteAll();

        IEnumerable<IEmployerBLL> GetAll();

        IEmployerBLL GetFreeOrNot(bool isFree, EmpPossition possition);

        IEmployerBLL GetById(int id);
    }
}
