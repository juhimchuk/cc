﻿using CallCenter.BLL.Interface.IService;
using CallCenter.BLL.Service;
using CallCenter.Data.Interface.IRepository;
using CallCenter.DataAccess.Repository;
using Ninject.Modules;

namespace CallCenter.IoC
{
    public class CallCenterNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IEmployerRepository)).To(typeof(EmployerRepository));

            Bind(typeof(IEmployerService)).To(typeof(EmployerService));
            Bind(typeof(ISeparateService)).To(typeof(SeparateService));

        }
    }
}
