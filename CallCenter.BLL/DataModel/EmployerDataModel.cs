﻿using CallCenter.Data.Interface;
using CallCenter.SharedEnum;

namespace CallCenter.BLL.DataModel
{
    public class EmployerDataModel : IEmployerData
    {
        public int Id { get; set; }

        public EmpPossition IdPossition { get; set; }

        public bool IsFree { get; set; }

    }
}
