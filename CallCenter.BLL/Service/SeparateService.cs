﻿using CallCenter.BLL.DataModel;
using CallCenter.BLL.Dto;
using CallCenter.BLL.Interface.IDto;
using CallCenter.BLL.Interface.IService;
using CallCenter.Data.Interface;
using CallCenter.Data.Interface.IRepository;
using CallCenter.SharedEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter.BLL.Service
{
    public class SeparateService : ISeparateService
    {
        private readonly IEmployerRepository _empRep;

        public SeparateService(IEmployerRepository empRep)
        {
            _empRep = empRep;
        }

        public IEmployerBLL SeperateCalls()
        {
           var op = _empRep.GetFreeOrNot(true, EmpPossition.Operator);
            if(op == null)
            {
                op = GetManager();
            }
            if (op is null)
            {
                return null;
            }
            var oper = EntityToDto( op);
            return oper;
        }

        private IEmployerData GetManager()
        {
            var man = _empRep.GetFreeOrNot(true, EmpPossition.Manager);
            if (man is null)
            {
                man = GetSenior();
            }

            return man;
        }

        private IEmployerData GetSenior()
        {
          return _empRep.GetFreeOrNot(true, EmpPossition.Senior);
            
        }
        

        private EmployerDto EntityToDto(IEmployerData model)
        {
            if (model == null)
                return null;
            return new EmployerDto
            {
                Id = model.Id,
                IdPossition = model.IdPossition,
                IsFree = model.IsFree
            };
        }

        public void EndCall(IEmployerBLL dto)
        {
            var emp = DtoToEntity(dto);
            emp.IsFree = true;
            _empRep.Update(DtoToEntity(emp));
        }


        private IEmployerData DtoToEntity(EmployerDto dto)
        {
            return new EmployerDataModel
            {
                Id = dto.Id,
                IdPossition = dto.IdPossition,
                IsFree = dto.IsFree
            };
        }

        private EmployerDto DtoToEntity(IEmployerBLL dto)
        {
            return new EmployerDto
            {
                Id = dto.Id,
                IdPossition = dto.IdPossition,
                IsFree = dto.IsFree
            };
        }
        
    }
}
