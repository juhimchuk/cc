﻿using CallCenter.BLL.DataModel;
using CallCenter.BLL.Dto;
using CallCenter.BLL.Interface.IDto;
using CallCenter.BLL.Interface.IService;
using CallCenter.Data.Interface;
using CallCenter.Data.Interface.IRepository;
using CallCenter.SharedEnum;
using System.Collections.Generic;
using System.Linq;

namespace CallCenter.BLL.Service
{
    public class EmployerService : IEmployerService
    {
        private readonly IEmployerRepository _empRep;

        public EmployerService(IEmployerRepository empRep)
        {
            _empRep = empRep;
        }

        public void AddRange(IEmployerInitBLL dto)
        {
            _empRep.DeleteAll();
            var emp = new List<EmployerDto>();

            for (int i = 0; i < dto.CountOperator; i++)
            {
                emp.Add(new EmployerDto { IdPossition = EmpPossition.Operator, IsFree = true });
            }
            for (int i = 0; i < dto.CountManager; i++)
            {
                emp.Add(new EmployerDto { IdPossition = EmpPossition.Manager, IsFree = true });
            }
            for (int i = 0; i < dto.CountSeniorManager; i++)
            {
                emp.Add(new EmployerDto { IdPossition = EmpPossition.Senior, IsFree = true });
            }
            
            _empRep.AddRange(DtoesToEntities(emp));
        }

        public void Add(IEmployerBLL dto)
        {
            _empRep.Add(DtoToEntity(dto));
        }

        public void Update(IEmployerBLL dto)
        {
            _empRep.Update(DtoToEntity(dto));
        }

        public void DeleteAll()
        {
            _empRep.DeleteAll();
        }

        public IEnumerable<IEmployerBLL> GetAll()
        {
            return EntitiesToDtoes(_empRep.GetAll());
        }

        public IEmployerBLL GetFreeOrNot(bool isFree, EmpPossition possition)
        {
            return EntityToDto(_empRep.GetFreeOrNot(isFree, possition));
        }

        public IEmployerBLL GetById(int id)
        {
            return EntityToDto(_empRep.GetById(id));
        }

        private IEnumerable<IEmployerBLL> EntitiesToDtoes(IEnumerable<IEmployerData> entity)
        {
            return entity.Select(emp => EntityToDto(emp));

        }

        private IEmployerBLL EntityToDto(IEmployerData model)
        {
            return new EmployerDto
            {
                Id = model.Id,
                IdPossition = model.IdPossition,
                IsFree = model.IsFree
            };
        }

        private IEmployerData DtoToEntity(IEmployerBLL dto)
        {
            return new EmployerDataModel
            {
                Id = dto.Id,
                IdPossition = dto.IdPossition,
                IsFree = dto.IsFree
            };
        }

        private IEnumerable<IEmployerData> DtoesToEntities(IEnumerable<IEmployerBLL> dtoes)
        {
            return dtoes.Select(emp => DtoToEntity(emp));

        }

    }
}
