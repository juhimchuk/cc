﻿using CallCenter.BLL.Interface.IDto;

namespace CallCenter.BLL.Dto
{
    public class EmployerInitDto : IEmployerInitBLL
    {
        public int CountOperator { get; set; }

        public int CountManager { get; set; }

        public int CountSeniorManager { get; set; }
    }
}
