﻿using CallCenter.SharedEnum;
using System.Collections.Generic;

namespace CallCenter.Data.Interface.IRepository
{
    public interface IEmployerRepository
    {
        void AddRange(IEnumerable<IEmployerData> dto);

        void Add(IEmployerData dto);

        void DeleteAll();

        IEnumerable<IEmployerData> GetAll();

        IEmployerData GetFreeOrNot(bool isFree, EmpPossition possition);

        IEmployerData GetById(int id);

        void Update(IEmployerData dto);

    }
}
