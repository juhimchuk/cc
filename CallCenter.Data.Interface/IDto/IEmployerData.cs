﻿using CallCenter.SharedEnum;

namespace CallCenter.Data.Interface
{
    public interface IEmployerData
    {
        int Id { get;  }

        EmpPossition IdPossition { get;  }

        bool IsFree { get;  }

    }
}
