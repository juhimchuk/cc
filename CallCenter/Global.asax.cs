﻿using CallCenter.IoC;
using CallCenter.Manager;
using CallCenter.Models;
using Newtonsoft.Json;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Text;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CallCenter
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var kernel = InitKernel();
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            SetupRabbitMqSubscriber();
        }

        public static IKernel InitKernel()
        {
            IKernel kernal = new StandardKernel(new CallCenterNinjectModule());
            return kernal;
        }

        private static IConnection connection;

        private static IModel channel;

        private void SetupRabbitMqSubscriber()
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                connection = factory.CreateConnection();
                channel = connection.CreateModel();
                //channel.QueueDeclare(queue: "rpc_queue", durable: false,
                //  exclusive: false, autoDelete: false, arguments: null);
                //channel.BasicQos(0, 1, false);
                var consumer = new EventingBasicConsumer(channel);
                channel.BasicConsume(queue: "rpc_queue",
                  autoAck: true, consumer: consumer);

                consumer.Received += (model, ea) =>
                {
                    byte[] response = null;

                    var body = ea.Body;
                    var props = ea.BasicProperties;
                    var replyProps = channel.CreateBasicProperties();
                    replyProps.CorrelationId = props.CorrelationId;
                    replyProps.Persistent = true;
                    try
                    {
                        var message = JsonConvert.DeserializeObject<CallModel>(Encoding.UTF8.GetString(body));
                        var con = new Consumer();
                        try { response = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(con.GetFreeEmployer(message))); }
                        catch (Exception e)
                        { }
                     
                    }
                    catch (Exception e)
                    {
                        response = Encoding.UTF8.GetBytes("");
                    }
                    finally
                    {
                        var responseBytes = response;
                        channel.BasicPublish(exchange: "", routingKey: props.ReplyTo,
                          basicProperties: replyProps, body: responseBytes);
                        // channel.BasicAck(deliveryTag: ea.DeliveryTag,
                        //  multiple: false);
                    }
                };
            }
            catch (Exception e)
            {

            }
            }
        
        }
    }
    


