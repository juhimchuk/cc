﻿using CallCenter.BLL.Interface.IService;
using CallCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallCenter.Controllers
{
    public class SettingController : Controller
    {
        private readonly IEmployerService _empService;

        public SettingController(IEmployerService empService)
        {
            _empService = empService;
        }

        public ActionResult EmployerSetting()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmployerSetting(EmployerModel employers)
        {
            _empService.AddRange(employers);
            return RedirectToAction("CallSetting");
        }

        public ActionResult CallSetting()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CallSetting(CallModel callsSet)
        {
            return View("CommandPrompt", callsSet);
        }

        public ActionResult StartSimulation()
        {
            return View();
        }

        public ActionResult Employers()
        {
            var emp = _empService.GetAll();
            return View();
        }
        
        
    }
}