﻿using CallCenter.BLL.Interface.IDto;
using CallCenter.BLL.Interface.IService;
using Ninject;

namespace CallCenter.Manager
{
    public class ManagerSeparateService
    {
        private IKernel _kernal;

        private ISeparateService sepService;

        public ManagerSeparateService() { }

        public ManagerSeparateService(IKernel kernal)
        {
            _kernal = kernal;
            sepService = _kernal.Get<ISeparateService>();
        }

        public IEmployerBLL SeperateCalls()
        {
            return sepService.SeperateCalls();
        }

        public void EndCall(IEmployerBLL emp)
        {
             sepService.EndCall(emp);
        }
    }
}
