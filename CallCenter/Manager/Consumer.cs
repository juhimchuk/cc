﻿using CallCenter.BLL.Interface.IDto;
using CallCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CallCenter.Manager
{
    public class Consumer
    {
        private readonly ManagerSeparateService _sepService;

        public Consumer()
        {
            _sepService = new ManagerSeparateService(MvcApplication.InitKernel());
        }

        public EmployerShowModel GetFreeEmployer(CallModel call)
        {
            return Swap(_sepService.SeperateCalls());
        }


        private EmployerShowModel Swap(IEmployerBLL emp)
        {
            return new EmployerShowModel
            {
                Id = emp.Id,
                IdPossition = emp.IdPossition,
                IsFree = emp.IsFree
            };
        }
    }
}