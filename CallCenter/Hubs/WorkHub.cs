﻿using CallCenter.BLL.Interface.IService;
using CallCenter.Manager;
using CallCenter.Models;
using CallCenter.SharedEnum;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CallCenter.Hubs
{
    public class WorkHub : Hub
    {
        private readonly ManagerSeparateService _sepService;

        private RpcClient _pub;

        public WorkHub()
        {
            _sepService = new ManagerSeparateService(MvcApplication.InitKernel());
            
        }

        public void Send(int call, int max, int min)
        {
            var calls = new List<CallModel>();
            for(int i = 0; i < call; i++)
            {
                calls.Add(new CallModel {MaxTimeResponse=max, MinTimeResponse=min });
            }
            Parallel.ForEach(calls, (currentcall) => GetMessage(currentcall));
           
        }

        public void GetMessage(CallModel call)
        {
            try
            {
                _pub = new RpcClient();
                var response = _pub.Call(call);
                string mess;
                if (response is null)
                {
                    mess = "Sorry! All operators are busy. Try again later.";
                    Clients.All.addMessage(mess);
                    Thread.Sleep(2000);
                    GetMessage(call);
                }
                else
                {
                    mess = String.Format("Hello! I’m {0} {1}.", Enum.GetName(typeof(EmpPossition), response.IdPossition), response.Id);
                }
                Clients.All.addMessage(mess);

                Thread.Sleep(new Random().Next(call.MinTimeResponse * 1000, call.MaxTimeResponse * 1000));

                mess = String.Format("{0} {1} ended a call.", Enum.GetName(typeof(EmpPossition), response.IdPossition), response.Id);
                Clients.All.addMessage(mess);
                 _sepService.EndCall(response); 
            
            }
            catch (Exception e)
            {
            }
           
        }
        
    }
}