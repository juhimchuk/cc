﻿

namespace CallCenter.Models
{
    public class CallModel 
    {
        public int CallNumber { get; set; }

        public int MinTimeResponse { get; set; }

        public int MaxTimeResponse { get; set; }
    }
}