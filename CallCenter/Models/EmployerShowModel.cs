﻿using CallCenter.BLL.Interface.IDto;
using CallCenter.SharedEnum;

namespace CallCenter.Models
{
    public class EmployerShowModel : IEmployerBLL
    {
        public int Id { get; set; }

        public EmpPossition IdPossition { get; set; }

        public bool IsFree { get; set; }
    }
}