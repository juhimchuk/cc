﻿using CallCenter.BLL.Interface.IDto;

namespace CallCenter.Models
{
    public class EmployerModel : IEmployerInitBLL
    {
        public int CountOperator { get; set; }

        public int CountManager { get; set; }

        public int CountSeniorManager { get; set; }
    }
}