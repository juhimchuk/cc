﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CallCenter.Startup))]
namespace CallCenter
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}