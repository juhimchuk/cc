namespace CallCenter.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employers", "IdPossition", "dbo.Possitions");
            DropIndex("dbo.Employers", new[] { "IdPossition" });
            DropTable("dbo.Possitions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Possitions",
                c => new
                    {
                        Title = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Title);
            
            CreateIndex("dbo.Employers", "IdPossition");
            AddForeignKey("dbo.Employers", "IdPossition", "dbo.Possitions", "Title", cascadeDelete: true);
        }
    }
}
