namespace CallCenter.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteLog : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.LogBooks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LogBooks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Log = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
