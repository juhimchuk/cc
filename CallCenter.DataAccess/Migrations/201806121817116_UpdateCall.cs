namespace CallCenter.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCall : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Calls");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallNumber = c.Int(nullable: false),
                        MinTimeResponse = c.Double(nullable: false),
                        MaxTimeResponse = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
