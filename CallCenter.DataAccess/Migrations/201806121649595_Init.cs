namespace CallCenter.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallNumber = c.Int(nullable: false),
                        MinTimeResponse = c.Double(nullable: false),
                        MaxTimeResponse = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdPossition = c.Int(nullable: false),
                        IsFree = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Possitions", t => t.IdPossition, cascadeDelete: true)
                .Index(t => t.IdPossition);
            
            CreateTable(
                "dbo.Possitions",
                c => new
                    {
                        Title = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Title);
            
            CreateTable(
                "dbo.LogBooks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Log = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employers", "IdPossition", "dbo.Possitions");
            DropIndex("dbo.Employers", new[] { "IdPossition" });
            DropTable("dbo.LogBooks");
            DropTable("dbo.Possitions");
            DropTable("dbo.Employers");
            DropTable("dbo.Calls");
        }
    }
}
