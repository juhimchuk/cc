﻿using CallCenter.Data.Interface;
using CallCenter.Data.Interface.IRepository;
using CallCenter.DataAccess.Dto;
using CallCenter.SharedEnum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CallCenter.DataAccess.Repository
{
    public class EmployerRepository : BaseRepository<Employer>, IEmployerRepository
    {
        public void AddRange(IEnumerable<IEmployerData> dto)
        {
            base.AddRange(DtoesToEntities(dto));
        }

        public void Add(IEmployerData dto)
        {
            base.Add(DtoToEntity(dto));
        }

        public void Update(IEmployerData dto)
        {
            base.Update(DtoToEntity(dto));
        }

        public void DeleteAll()
        {
            base.DeleteAll();
        }

        public IEnumerable<IEmployerData> GetAll()
        {
            return EntitiesToDtoes(base.GetAll());
        }

        public IEmployerData GetFreeOrNot(bool isFree, EmpPossition possition)
        {
            var employer = base.GetSingleByFunc(emp => (emp.IsFree == isFree) && (emp.IdPossition == possition));
            if(employer != null)
            {
                employer.IsFree = false;
                base.Update(employer);
                return EntityToDto(employer);
            }
            return null;
           
           
        }

        public IEmployerData GetById(int id)
        {
            return EntityToDto(base.Get(id));
        }

        private IEnumerable<IEmployerData> EntitiesToDtoes(IEnumerable<Employer> entity)
        {
            return entity.Select(emp => EntityToDto(emp));

        }

        private IEmployerData EntityToDto(Employer model)
        {
            return new EmployerDto
            {
                Id = model.Id,
                IdPossition = model.IdPossition,
                IsFree = model.IsFree
            };
        }

        private Employer DtoToEntity(IEmployerData dto)
        {
            return new Employer
            {
                Id = dto.Id,
                IdPossition = dto.IdPossition,
                IsFree = dto.IsFree
            };
        }

        private IEnumerable<Employer> DtoesToEntities(IEnumerable<IEmployerData> dtoes)
        {
            return dtoes.Select(emp => DtoToEntity(emp));

        }

    }
}
