﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter.DataAccess.Repository
{
   public class BaseRepository<T> where T : class
    {
        protected IEnumerable<T> GetAll()
        {
           
            using (var context = new CallContext())
            {
                return context.Set<T>().ToList();
            }
        }

        protected IEnumerable<T> GetByFunc(Func<T, bool> predicate)
        {

            using (var context = new CallContext())
            {
                return context.Set<T>().Where(predicate).ToList();
            }
        }

        protected T GetSingleByFunc(Func<T, bool> predicate)
        {

            using (var context = new CallContext())
            {
                return context.Set<T>().FirstOrDefault(predicate);
            }
        }

        protected T GetFirst()
        {

            using (var context = new CallContext())
            {
                return context.Set<T>().FirstOrDefault();
            }
        }

        protected void AddRange(IEnumerable<T> item)
        {

            using (var context = new CallContext())
            {
                 context.Set<T>().AddRange(item);
                context.SaveChanges();
            }
        }

        protected void Add(T item)
        {

            using (var context = new CallContext())
            {
                context.Set<T>().Add(item);
                context.SaveChanges();
            }
        }

        protected T Get(int id)
        {
            using (var context = new CallContext())
            {
                return context.Set<T>().Find(id);
            }
        }

        protected void DeleteAll()
        {
            using (var context = new CallContext())
            {
                context.Set<T>().RemoveRange(context.Set<T>());
                context.SaveChanges();
            }
        }

        public void Update(T item)
        {
            using (var context = new CallContext())
            {
                context.Entry(item).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
