﻿using CallCenter.Data.Interface;
using CallCenter.SharedEnum;

namespace CallCenter.DataAccess.Dto
{
    public class EmployerDto : IEmployerData
    {
        public int Id { get; set; }

        public EmpPossition IdPossition { get; set; }

        public bool IsFree { get; set; }
        
    }
}
