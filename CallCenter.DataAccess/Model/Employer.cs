﻿using CallCenter.SharedEnum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CallCenter.DataAccess
{
    public class Employer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public EmpPossition IdPossition { get; set; }

        public bool IsFree { get; set; }
        

    }
}
