﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter.DataAccess
{
    public class CallContext : DbContext
    {
        private const string CONNECTION_STRING_NAME = "CallCenterContext";

        public CallContext() : base(CONNECTION_STRING_NAME)
        {
        }

        public DbSet<Employer> Employer { get; set; }
    }
}
