﻿namespace CallCenter.SharedEnum
{
    public enum EmpPossition
    {
        Operator,
        Manager,
        Senior
    }
}
